const validation = () => {
  let fullName = document.getElementById("fullName").value;
  let password = document.getElementById("password").value;

  const fullNameCheck = /^[A-Za-z. ]{3,30}$/;
  const emailCheck = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
  const passwordCheck = /^(?=.*?[A-Za-z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/;

  if (fullNameCheck.test(fullName) || fullNameCheck.test(emailCheck)) {
    document.getElementById("full__name__error").innerHTML = "";
  } else {
    document.getElementById("full__name__error").innerHTML =
      "**Nhập không hợp lệ. Phải bắt đầu bằng chữ, tối thiểu 3 ký tự, tối đa 30 ký tự, không có số, không có ký tự đặc biệt.";
    return false;
  }
  if (passwordCheck.test(password)) {
    document.getElementById("password__error").innerHTML = "";
  } else {
    document.getElementById("password__error").innerHTML =
      "**Password không hợp lệ. Phải có ít nhất một ký tự viết hoa, một ký tự đặc biệt, một ký tự số. ";
    return false;
  }
};
